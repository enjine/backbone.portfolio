<?php
date_default_timezone_set ("America/New_York");

logAccess();

function logAccess($status = 200) {
	file_put_contents("php://stdout", sprintf("[%s] %s:%s [%s]: %s\n",
		date("D M j H:i:s Y"), $_SERVER["REMOTE_ADDR"],
		$_SERVER["REMOTE_PORT"], $status, $_SERVER["REQUEST_URI"]));
}

function logMessage($message) {
	file_put_contents("php://stdout", sprintf("[%s] %s:%s: %s\n",
		date("D M j H:i:s Y"), $_SERVER["REMOTE_ADDR"],
		$_SERVER["REMOTE_PORT"], $message));
}


function json_get_last_error_msg(){
	switch (json_last_error()) {
		case JSON_ERROR_NONE:
			$msg = 'No errors';
			break;
		case JSON_ERROR_DEPTH:
			$msg = 'Maximum stack depth exceeded';
			break;
		case JSON_ERROR_STATE_MISMATCH:
			$msg = 'Underflow or the modes mismatch';
			break;
		case JSON_ERROR_CTRL_CHAR:
			$msg = 'Unexpected control character found';
			break;
		case JSON_ERROR_SYNTAX:
			$msg = 'Syntax error, malformed JSON';
			break;
		case JSON_ERROR_UTF8:
			$msg = 'Malformed UTF-8 characters, possibly incorrectly encoded';
			break;
		default:
			$msg = 'Unknown error';
			break;
	}
	return $msg;
}

function executeRoute(){
	$routeHandler = $_SERVER['SCRIPT_FILENAME'];
	logMessage("Executing route: ". $routeHandler);
	require_once($routeHandler);
}

executeRoute();

