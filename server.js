'use strict';

var express = require('express');
//var http = require('http');
var path = require('path');
var async = require('async');
//var hbs = require('express-hbs');




// init express
var app = express();

app.set('port', process.env.PORT || 3000);
//app.set('view engine', 'handlebars');
//app.set('views', __dirname + 'app/modules/views');

// set logging
app.use(function(req, res, next){
  console.log('%s %s', req.method, req.url);
  next();
});

// mount static
var staticPath = path.resolve(__dirname);
app.use(express.static( staticPath ));
//app.use(express.static( path.join( __dirname, '../.tmp') ));


// route index.html
app.get('/', function(req, res){
  res.sendFile( path.join( __dirname, 'index.html' ) );
});
// set logging
app.use('/*', function(req, res, next){
  res.sendFile( path.join( __dirname, 'index.html' ) );
});

// start server
app.listen(app.get('port'), function(){
    console.log('Express App started!');
});



