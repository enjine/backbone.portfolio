define(['backbone', 'modules/router', 'modules/controller'],
	function (Backbone, AppRouter, AppController) {
		'use strict';

		var App = {},
			JST = window.JST = window.JST || {};

		App = Backbone.Marionette.Application.extend({
			initialize: function (options) {
				//console.log('App Init!', arguments);
				Backbone.Marionette.Application.prototype.initialize.apply(this, arguments);
			},

			start: function (options) {
				Backbone.Marionette.Application.prototype.start.apply(this, arguments);
			},

			onBeforeStart: function(){
				//console.log('before:start', this);
				this.container = this.options.config.container;

				// Start Backbone history so we have bookmarkable URL's
				Backbone.history.start({
					pushState: true,
					root: this.options.config.root
				});

				// initialize the router
				this.router = new AppRouter({
					isFirstRoute: true,
					root: this.options.config.root,
					controller: new AppController({
						config: this.options.config.root,
						data: this.options.models
					})
				});
			},

			onStart: function () {
				//console.log('start', this);
				//console.log('App container:', this.container);
			},

		});


		Backbone.Marionette.Renderer.render = function (template, data) {
			if (!JST[template]) {
				throw 'Template "' + template + '" not found!"';
			}
			//console.log('----', template, data);
			return JST[template](data);
		};


		return App;
	});
