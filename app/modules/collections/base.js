define(['backbone','models/base'], function(Backbone, BaseModel){
	'use strict';

	return Backbone.Collection.extend({
		model: BaseModel,

		initialize: function(options){
			Backbone.Collection.prototype.initialize.apply(this, arguments);
		}
	});
});
