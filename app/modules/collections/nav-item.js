define(['backbone', './base'],
	function(Backbone, BaseCollection, NavItem){
	'use strict';

	return BaseCollection.extend({

		initialize: function(models, options){
			BaseCollection.prototype.initialize.apply(this, arguments);
		}
	});
});
