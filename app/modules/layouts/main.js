define(['backbone', 'marionette' ,'underscore','modules/filters/route/hybrid'],
	function(Backbone, Marionette, _, RouteFilter){
	'use strict';

	return Marionette.LayoutView.extend({
		el: '#app',
		template: 'app/templates/layouts/main.hbs',
		regions: {
			'header' : {
				selector: 'nav.main'
			},
			'content-inner': {
				selector: 'main .content-inner'
			}
		},
		events: {
			// this hijacks all 'internal' links and sends them to the router
			// adding a [data-bypass] attribute will allow the link to be handled normally.
			// TODO: consider changing the selector to a faster one (.norouter) ?
			'click a:not(a[data-bypass])': 'onRouterEvent'
		},

		initialize: function(options){
			this.baseUrl = options.baseUrl;
			// TODO: figure out if you need to do this... no?
			//Backbone.Marionette.LayoutView.prototype.initialize.apply(this, arguments);
		},

		onRouterEvent: function(e){
			var filter = new RouteFilter(e);

			return filter.exec();
		}
	});
});
