define(['backbone', 'marionette' ,'underscore'], function(Backbone, Marionette, _){
	'use strict';

	return Backbone.Marionette.LayoutView.extend({
		el: 'main .content-inner',
		template: 'app/templates/layouts/home/main.hbs',
		regions: {
			'content-inner': {
				selector: '.mainstuff'
			}
		}
	});

});
