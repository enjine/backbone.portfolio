define(['backbone', 'marionette', 'app', 'modules/router', 'layouts/main', 'routes/index', 'routes/work','routes/sandbox','routes/contact', 'views/navigation'],
	function (Backbone, Marionette, App, Router, MainLayout, HomeRoute, WorkRoute, SandboxRoute, ContactRoute, Navigation) {

		var AppController = Marionette.Object.extend({
			config: null,
			data: null,

			initialize: function(options){
				this.config = options.config;
				this.data = options.data;
				console.log('MainController INIT', this, arguments);

				this.loadSubApp.bind(this);
				this.sandbox.bind(this);
				this.contact.bind(this);
				this.work.bind(this);
				this.home.bind(this);
			},

			/**
			 * Renders the master view of which all other views are children.
			 */
			firstRoute: function(){
				console.log('initiaRender', this, arguments);
				var layout = new MainLayout({baseUrl: this.config});

				layout.render();

				layout.getRegion('header').show(new Navigation({
					branding: this.data.branding,
					menu: this.data.navigation.main
				}));
			},

			unhandled: function(){
				console.error('No Route Handler Defined', this, arguments);
			},

			home: HomeRoute,

			work: WorkRoute,

			contact: ContactRoute,

			sandbox: function(){
				console.log('SANDBOX', this, arguments);
			},

			loadSubApp: SandboxRoute
		});

		return AppController;
	});
