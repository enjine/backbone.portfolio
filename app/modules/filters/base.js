define(['marionette'], function(Marionette){
	'use strict';

	var BaseFilter = Marionette.Object.extend({
		constants: {
			TYPE: null
		},

		initialize: function(event, options){
			Marionette.Object.prototype.initialize.apply(this, arguments);
		},

		exec: function(){

		}

	});


	return BaseFilter;

});
