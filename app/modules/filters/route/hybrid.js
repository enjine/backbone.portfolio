define(['../base','underscore', 'backbone'],function(BaseFilter, _, Backbone){
	'use strict';

	return BaseFilter.extend({
		allowedProtocols: [
			'http'
		],


		initialize: function(event, options){
			this.mergeOptions(options);

			this.constants.TYPE = 'route.hybrid';
			this.ev = event;
			var target = this.ev.target;

			this.root = Backbone.history.root;
			this.href = target.href;
			this.protocol = target.protocol.split(':')[0];
			this.pathname = target.pathname;
			this.isProtocolAllowed = this.allowedProtocols.indexOf(this.protocol) !== -1;
			this._parseDataset(target);

			//console.log('Internal Link Clicked: ', this.ev, target, this, this.root, this.href, this.protocol, this.pathname, this.isProtocolAllowed);

			BaseFilter.prototype.initialize.apply(this, arguments);

		},

		_parseDataset: function(el){
			//console.log('dataset:', this, el.dataset);
			// cancel this behavior entirely with: data-nopushstate
			this.isPushState = (el.dataset.nopushstate) ? false : true;
			if(el.dataset.overrides instanceof Object){
				// override default functionality data-override="{key:values, for:callback}"
				this.overrides = JSON.parse(el.dataset.overrides);
			}else{
				this.overrides = null;
			}
		},

		_shouldEngage: function(){
			return (!this.ev.isDefaultPrevented() && this.isProtocolAllowed && this.isPushState);
		},

		_isOverridden: function(){
			return (null !== this.overrides && this.overrides instanceof Object);
		},

		_isLocalPath: function(regex, pathFrag){
			return regex.test(pathFrag);
		},

		_passthru: function(){
			this.ev.appliedFilters = [this.constants.TYPE];
			return this.ev;
		},

		_handleOverride: function(params){
			this.ev.preventDefault();
			this.ev.overrides = params;
			this.trigger('route:override', this.ev);
		},


		exec: function(){
			var pathFrag = this.pathname.replace(/^\//, '').replace(/\/$/, ''),
				cleanRoot = this.root.replace(/^\//, '').replace(/\/$/, ''),
				isRootRegex = new RegExp('^' + cleanRoot),
				routeFrag = this.pathname.split(cleanRoot+'/')[1].replace('/',''),
				existingRouteHandlers = Backbone.history.handlers,
				dest;

			if (this._shouldEngage()) {
				if(!this._isOverridden()) {
					if (this._isLocalPath(isRootRegex, pathFrag)) {
						this.ev.preventDefault();
						_(existingRouteHandlers)
							.chain()
							.pluck('route')
							.each(function(route) {
								if (route.test(pathFrag) || pathFrag === this.root) {
									event.preventDefault();
									dest = routeFrag.length ? pathFrag : this.root;
									Backbone.history.navigate(dest, {trigger: true});
								}
							}.bind(this));
					} else{
						return true;
					}
				}else{
					return this._handleOverride(this.overrides);
				}
			}

			return this._passthru();
		}
	});
});
