define(['backbone', 'marionette', 'underscore', 'qa.models/q'], function(Backbone, _, QuestionModel){

	var QuestionView = Backbone.Marionette.ItemView.extend({
		model: QuestionModel,

		initialize: function(){
			Backbone.Marionette.ItemView.prototype.initialize.apply(this, arguments);
			this.listenTo(this.model, 'change', this.onModelChange, this);
		},

		onModelChange: function(){
			console.log('model changed', this.model.serialize(), arguments);
			this.render();
		},

		render: function(){

			console.log('rendering  question....', arguments);
		}
	});

	return QuestionView;
});
