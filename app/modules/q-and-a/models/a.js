define(['backbone'], function(Backbone){

	var Answer = Backbone.Model.extend({
		defaults: {
			id: 0,
			question_id: null,
			value: null,
			timestamp: null
		}
	});

	return Answer;
});
