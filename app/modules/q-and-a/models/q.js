define(['backbone','underscore'], function(Backbone, _){

	var QuestionModel = Backbone.Model.extend({
		defaults: {
			imageId: null,
			thumbnailSrc: null,
			mainImgSrc: null,
			href: null,
			text: null
		},

		parse: function(response, options){
			if(options.collection){
				return response;
			}
			this.imageId = response.imageId;
			this.thumbnailSrc = response.tbUrl;
			this.mainImgSrc = response.url;
			this.href = response.originalContextUrl;
			this.text = 'Cat or not cat?';
		},

		/**
		 * Returns data for templates
		 *
		 * @returns {*}
		 */
		serializeData: function(){
			return _.extend(_.clone(this.attributes), {

			});
		},

		/**
		 * Returns data for persistence/storage
		 *
		 * @returns {*}
		 */
		getJSON: function(){
			return Backbone.Model.prototype.getJSON.apply(this, arguments);

		}
	});

	return QuestionModel;
});
