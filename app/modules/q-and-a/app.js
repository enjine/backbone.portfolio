define(['underscore', 'backbone', 'qa.models/q', 'qa.models/a','qa.views/q'],
	function(_, Backbone, QuestionModel, AnswerModel, QuestionView){
	'use strict';

	var QCollection = Backbone.Collection.extend({
		model: QuestionModel,

		pagination: {},
		responseDetails: '',
		responseStatus: null,

		initialize: function(options){
			Backbone.Collection.prototype.initialize.apply(this, arguments);
			this.url  = options.url;
			console.log('QCollection init!', arguments, this.model);
		},

		fetch: function(){
			Backbone.Collection.prototype.fetch.apply(this, arguments);
			console.log('QCollection fetch!', arguments);
		},

		parse: function(response, options){
			if(options.xhr.status === 200){
				var payload = response.responseData || [];
				var result = payload.results;

				this.pagination = payload.cursor;
				this.responseDetails = payload.responseDetails;
				this.responseStatus = payload.responseStatus;

				return result;
			}else{
				switch(options.xhr.status){
					case 204:
						this.trigger('noContent', response, options);
						break;
					default:
						console.error('Unhandled XHR Status code:' + options.xhr.status);
						break;
				}
			}
		}
	});

	var MainView = Backbone.Marionette.CompositeView.extend({
		className: 'q-and-a',
		template: 'app/modules/q-and-a/templates/main.hbs',

		initialize: function(options){
			Backbone.Marionette.CompositeView.prototype.initialize.apply(this, arguments);
			console.log('QAndA View init!', this, arguments);
			this.model = new Backbone.Model(options.data);
		},

		_onSuccess: function(collection, response, xhr){
			console.log('_onSuccess', collection, response, xhr);
			this.update();
		},

		update: function(){
			console.log(this, arguments);
			this.$el.append('<h1>Yay DATA!</h1>');
		},

		_onError: function(){
			console.log('_onError', arguments);
		},

		onShow: function(view, region){
			console.log('QAndAMainView show', this, arguments);
			this.collection = new QCollection({url: view.options.collectionUrl});

			this.collection.fetch({
				reset: true,
				data: {
					q: 'cat'
				},
				success: this._onSuccess.bind(this),
				error: this._onError.bind(this)
			});

		},

		render: function(){
			console.log('QAndAMainView render', this, arguments);
			Backbone.Marionette.CompositeView.prototype.render.apply(this, arguments);
		}
	});

	var QAndAApp = new Backbone.Marionette.Application.extend({
		initialize: function(options){
			Backbone.Marionette.Application.prototype.initialize.apply(this, arguments);
			console.log('QAndA: INIT!', options);
		},

		start: function(options){
			Backbone.Marionette.Application.prototype.start.call(this, options);
			console.log('QAndA: START!', options);
		}
	});

	return MainView;
});
