define(['backbone'], function(Backbone){
	'use strict';

	return Backbone.Model.extend({

		initialize: function(){
			Backbone.Model.prototype.initialize.apply(this, arguments);
		}

	});

});
