define(['backbone','./base', 'collections/nav-item'],
	function(Backbone, BaseModel, NavItemCollection){
	'use strict';

	return BaseModel.extend({

		defaults: {
			name: null,
			href: null,
			text: null,
			title: null,
			sections: NavItemCollection

		},

		initialize: function(model, options){
			if(model.sections && model.sections.length){
				this.set('sections', new NavItemCollection(model.sections));
			}
			BaseModel.prototype.initialize.apply(this, arguments);
		}

	});
});
