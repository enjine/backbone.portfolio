define(['backbone','./base'], function(Backbone, BaseModel){
	'use strict';

	return BaseModel.extend({

		initialize: function(){
			BaseModel.prototype.initialize.apply(this, arguments);
		}
	});
});

