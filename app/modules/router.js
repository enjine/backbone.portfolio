define(['backbone', 'underscore', 'marionette', 'backbone.radio'],
	function (Backbone, _, Marionette, Radio) {
		'use strict';

		var AppRouter = Backbone.Marionette.AppRouter.extend({
			appRoutes: {
				'*actions*params': 'unhandled', // matches http://example.com/#anything-here?#any-params
				'': 'home',
				'work': 'work',
				'contact': 'contact',
				'sandbox': 'sandbox',
				'sandbox/:app': 'loadSubApp'
			},

			initialize: function(options){
				this.isFirstRoute = options.isFirstRoute || false;
				this.channel = Radio.channel('router');
				console.log('AppRouter init.', this, arguments);
				this.on('all', this._onRouterEvent);
				this.channel.comply('filter:add', this._onAddFilter)
				this.channel.comply('handler:add', this._onAddHandler)

				this.listenTo(this.channel, 'auth:success', this._onAuthSuccess)

				this.listenTo(Backbone.history, 'route', this._onHistoryRoute);

				this.once('route', function(){
					this.isFirstRoute = false;
				});


				Backbone.Marionette.AppRouter.prototype.initialize.apply(this, arguments);
				if(this.isFirstRoute){
					this.options.controller.firstRoute();
				}
			},

			_onAuthSuccess: function(name, path, args) {
				console.log('_onAuthSuccess:', this, name, path, args);
			},

			_onAddHandler: function(name, path, args) {
				console.log('_onAddHandler:', this, name, path, args);
			},

			_onAddFilter: function(name, path, args) {
				console.log('_onAddFilter:', this, name, path, args);
			},

			_onRouterEvent: function(name, path, args) {
				console.log('OnRouterEvent:', this, name, path, args);
			},

			_onHistoryRoute: function(name, path, args) {
				console.log('OnHistoryRoute:', this, name, path, args);
			}
		});

		return AppRouter;
	});
