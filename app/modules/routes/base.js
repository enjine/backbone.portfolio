define(['views/base'], function(BaseView){
	'use strict';

	var HomeRouteHandler = function(){
		console.log('home route handler');

		if (this.home === null) {
			this.home = new BaseView({ model: null });
		}

		this.container.myChildView = this.home;
		this.container.render();

	};

	return HomeRouteHandler;
});
