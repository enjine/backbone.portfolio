define(['backbone','marionette','modules/q-and-a/app', 'layouts/sandbox/main'],
function(Backbone, Marionette, QAndAApp, SandboxMainLayout){
	'use strict';

	var SandboxRouteHandler = function(subAppName){
		console.log('-SANDBOX-', arguments, this);

		//create new layout view

		// generic view: quick and simple
		/*var layout = new Marionette.LayoutView({
			el: 'main .content-inner',
			template: 'app/templates/layouts/sandbox/main.hbs',
			regions: {
				'content-inner': {
					selector: '.mainstuff'
				}
			}
		});*/

		// or if you have a custom view in a separate module.
		var layout = new SandboxMainLayout();

		layout.render();

		layout.getRegion('content-inner').show(new QAndAApp({
			data: {
				greeting: 'its happening...',
				success: 'YAY DATA!'
			},
			collectionUrl: 'http://localhost:8000/api'
		}));

	};

	return SandboxRouteHandler;
});
