define(['marionette'], function(Marionette){
	'use strict';

	var ContactRouteHandler = function(){
		console.log('-CONTACT-', arguments, this);

		var layout = new Marionette.LayoutView({
			el: 'main .content-inner',
			template: 'app/templates/layouts/contact/main.hbs',
			regions: {
				'content-inner': {
					selector: '.mainstuff'
				}
			}
		});

		layout.render();

	};

	return ContactRouteHandler;
});
