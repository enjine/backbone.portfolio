define(['marionette'], function(Marionette){
	'use strict';

	var WorkRouteHandler = function(){
		console.log('-WORK-', arguments, this);

		var layout = new Marionette.LayoutView({
			el: 'main .content-inner',
			template: 'app/templates/layouts/work/main.hbs',
			regions: {
				'content-inner': {
					selector: '.mainstuff'
				}
			}
		});

		layout.render();

	};

	return WorkRouteHandler;
});
