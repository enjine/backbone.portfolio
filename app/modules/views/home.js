define(['backbone','marionette'], function(Backbone){
	'use strict';

	var HomeView = Backbone.Marionette.View.extend({

		initialize: function(){
			Backbone.Marionette.View.prototype.initialize.apply(this, arguments);
			console.log('HomeView init!');
		}
	});

	return HomeView;

});
