define(['backbone', 'marionette', 'models/branding', 'models/nav-item', 'collections/nav-item'],
	function(Backbone, Marionette, BrandingModel, NavItemModel, NavItemCollection){
	'use strict';

	var BrandingItemView = Backbone.Marionette.ItemView.extend({
		template: 'app/templates/navigation/header.hbs',
		className: 'branding'

	});

	// this is where it all happens
	var NavigationItemView = Marionette.CompositeView.extend({
		template: 'app/templates/navigation/item.hbs',
		model: NavItemModel,
		itemView: NavigationItemView,

		tagName: 'li',


		initialize: function () {
			Marionette.CompositeView.prototype.initialize.apply(this, arguments);

			var subsections = this.model.get('sections');

			if(subsections && subsections.length > 0){
				if(subsections instanceof Backbone.Collection === false){
					subsections = new NavItemCollection(subsections);
				}
				this.collection = subsections;
			}

		},

		initialEvents: function() {
			// Bind to any changes in the collection and redraw the entire menu chain
			if (this.collection) {
				this.listenTo(this.collection, 'add', this.render, this);
				this.listenTo(this.collection, 'remove', this.render, this);
				this.listenTo(this.collection, 'reset', this.render, this);
			}
		},

		attachHtml: function(collectionView, itemView){
			var nodeType,
				$appendToEl = collectionView.$('ul:first'),
				childListEl = document.createElement('ul'),
				nestedListClassName = 'nested',
        		appendMe;

			if(typeof this.collection === 'undefined'){
				nodeType = 'leaf';
			}else{
				nodeType = 'branch';
			}


	 		if ($appendToEl.length === 0) {
	 			$appendToEl = collectionView.$el;
				childListEl.className = nestedListClassName;

				if (this.isFirstLevel) {
					childListEl.className = 'hidden';
				}

				childListEl.appendChild(itemView.el);
				appendMe = childListEl;

			}else{
				appendMe = itemView.el;
			}

			$appendToEl.append(appendMe);
		}
	});

	var RecursiveListView = Marionette.CollectionView.extend({
		template: 'app/templates/navigation/main.hbs',
		tagName: 'ul',

	});

	var NavigationRootView = RecursiveListView.extend({
		isRootLevel: true,
		itemView: NavigationItemView,
		className: 'nav navbar-nav root'

	});

	var NavigationLayout = Marionette.LayoutView.extend({
		template: 'app/templates/navigation/layouts/main.hbs',
		el: 'nav.main > .container',
		regions: {
			'header' : {
				selector: '.navbar-header'
			},
			'menu': {
				selector: '.navbar-main-collapse'
			}
		},

		initialize: function(options){
			Backbone.Marionette.LayoutView.prototype.initialize.apply(this, arguments);

			this.branding = new BrandingItemView({
				model: new BrandingModel(options.branding)
			});

			this.menu = new NavigationRootView({
				collection: new NavItemCollection(options.menu, {model: NavItemModel}),
				childView: NavigationItemView
			});

		},

		onShow: function() {
			this.getRegion('header').show(this.branding);
			this.getRegion('menu').show(this.menu);

		}
	});

	return NavigationLayout;
});

