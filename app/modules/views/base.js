define(['backbone','marionette'], function(Backbone){
	'use strict';

	var BaseView = Backbone.Marionette.ItemView.extend({

		initialize: function(){
			Backbone.Marionette.View.prototype.initialize.apply(this, arguments);
			console.log('BaseView init!');
		}
	});

	return BaseView;

});
