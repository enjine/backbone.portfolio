require.config({
	baseUrl: 'app',
    paths: {
		'router'				: 'modules/router',
		'app'					: 'app',
		'templates'				: '../build/templates',
		'jquery'				: '../bower_components/jquery/dist/jquery',
		'backbone'				: '../bower_components/backbone/backbone',
		'underscore'			: '../bower_components/lodash/dist/lodash',
		'marionette'			: '../bower_components/marionette/lib/core/backbone.marionette',
		'backbone.babysitter'	: '../bower_components/backbone.babysitter/lib/backbone.babysitter',
		'backbone.wreqr'		: '../bower_components/backbone.wreqr/lib/backbone.wreqr',
		'backbone.radio'		: '../bower_components/backbone.radio/build/backbone.radio',
		'bootstrap'				: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap',
		'handlebars'			: '../bower_components/handlebars/handlebars',
		'bb.super'				: '../bower_components/backbone-super/backbone-super/backbone-super',
		'modules'				: 'modules',
		'layouts'				: 'modules/layouts',
		'views'					: 'modules/views',
		'models'				: 'modules/models',
		'collections'			: 'modules/collections',
		'routes'				: 'modules/routes',
		'qa.models'				: 'modules/q-and-a/models',
		'qa.views'				: 'modules/q-and-a/views'
	},
	shim : {
		jquery : {
			exports : 'jQuery'
		},
		underscore : {
			exports : '_'
		},
		backbone : {
			deps : ['jquery', 'underscore'],
			exports : 'Backbone'
		},
		marionette : {
			deps : ['jquery', 'underscore', 'backbone'],
			exports : 'Marionette'
		},
		bootstrap : {
			deps : ['jquery']
		},
		app : {
			deps : ['jquery', 'underscore', 'backbone', 'marionette']
		},
		router : {
			deps : ['app']
		},
		templates : {
			deps : ['handlebars']}
		}
});

require([
	'backbone',
	'app',
	'templates',
	'handlebars',
	'bootstrap',
],
function(Backbone, App) {
	'use strict';

	var app = new App(window.DS);
	app.start();

});
