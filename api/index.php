<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$IP = "24.115.247.53";

if (isset($_REQUEST['q'])) {

	$start = (isset($_REQUEST['start']))?"&start=".$_REQUEST['start'] : "";

	$url = "https://ajax.googleapis.com/ajax/services/search/images?" .
		"v=1.0&q=" . $_REQUEST['q'] . "&userip=" . $IP.$start;

	logMessage("API Requested: ".$url);

// sendRequest
// note how referer is set manually
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_REFERER, $IP);
	$json = curl_exec($ch);
	curl_close($ch);

	header("Access-Control-Allow-Origin: *");

// now, process the JSON string
	if(json_decode($json)){
		// now have some fun with the results...
		header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK', true, 200);
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		die($json);
	}else{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		die(json_get_last_error_msg());
	}


}

